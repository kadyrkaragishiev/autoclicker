using EnemyFactory;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PlayerBehaviour
{
    public class Player : MonoBehaviour
    {

        private void Update()
        {
            //Click on the screen to damage the enemy
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    Enemy component = hit.transform?.GetComponent<Enemy>();
                    if (component != null) component.Damage(1, hit.point);
                }
            }
        }
    }
}
