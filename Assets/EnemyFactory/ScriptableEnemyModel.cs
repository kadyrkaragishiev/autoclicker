using EnemyFactory.DeathBehaviours;
using UnityEngine;

namespace EnemyFactory
{
    [CreateAssetMenu(fileName = "Enemy", menuName = "EnemyFactory/Enemy", order = 1)]
    public class ScriptableEnemyModel : ScriptableObject
    {
        public string name;
        public int health;
        public float speed;
        public GameObject EnemyPrefab;
    }
}
