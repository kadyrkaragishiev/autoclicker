using System;
using EnemyFactory.DeathBehaviours;
using UnityEngine;

namespace EnemyFactory
{
    public abstract class Enemy : MonoBehaviour
    {
        [SerializeField]
        private AbstractDeathBehaviour _deathBehaviour;

        [SerializeField]
        private GameObject hit;
        private string _name;
        private int _health;
        private float _speed;
        private GameObject _enemyViewPrefab;
        public event Action OnDie;


        public void SetModel(ScriptableEnemyModel enemyModel)
        {
            _name = enemyModel.name;
            _health = enemyModel.health;
            _speed = enemyModel.speed;
            _enemyViewPrefab = enemyModel.EnemyPrefab;
        }

        private void ChangeHealth(int delta)
        {
            _health += delta;
            if (_health <= 0)
            {
                DeathBehaviour();
                OnDie?.Invoke();
            }
        }

        protected virtual void DamageVisualization() => Debug.Log("Damage"); //TODO: Add damage visualization
        protected virtual void DeathBehaviour() => _deathBehaviour.Die(); //TODO: Add death behaviour

        // ReSharper disable Unity.PerformanceAnalysis
        public void Damage(int damage, Vector3 position)
        {
            ChangeHealth(-damage);
            DamageVisualization();
            Instantiate(hit, position, Quaternion.identity);
        } 
    }
}
