using UnityEngine;

namespace EnemyFactory.DeathBehaviours
{
    public class AbstractDeathBehaviour : MonoBehaviour, IDeathBehaviour
    {
        public void Die()
        {
            AnimateDeath();
            Destroy(gameObject);  
        } 

        protected virtual void AnimateDeath()
        {
        }
    }
}
