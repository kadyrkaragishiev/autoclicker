using System;

namespace EnemyFactory.DeathBehaviours
{
    public interface IDeathBehaviour
    {
        void Die();
    }
}
