using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace EnemyFactory
{
    public class Fabric : MonoBehaviour
    {
        [SerializeField]
        private List<ScriptableEnemyModel> _enemyModels;

        [SerializeField]
        private Vector3 center, size;

        [SerializeField]
        private float spawnRate;

        private int _enemyCount = 0;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(center, size);
        }

        private void Start()
        {
            StartCoroutine(EnemySpawner());
            for (var i = 0; i < _enemyModels.Count; i++) CreateEnemy(i);
        }

        private IEnumerator EnemySpawner()
        {
            //infinite loop to spawn enemies
            while (true)
            {
                yield return new WaitForSeconds(spawnRate);
                CreateEnemy(Random.Range(0, _enemyModels.Count));
                if (_enemyCount >= 10)
                    UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            }
        }

        private void CreateEnemy(int index)
        {
            Vector3 pos = center + new Vector3(Random.Range(-size.x / 2, size.x / 2),
                Random.Range(-size.y / 2, size.y / 2), Random.Range(-size.z / 2, size.z / 2));
            GameObject enemy = Instantiate(_enemyModels[index].EnemyPrefab, pos, Quaternion.identity);
            enemy.GetComponent<Enemy>().SetModel(_enemyModels[index]);
            enemy.GetComponent<Enemy>().OnDie += () => _enemyCount--;
            _enemyCount++;
            
        }

        public ScriptableEnemyModel GetEnemyModel(int index) => _enemyModels[index];
    }
}
